<?php

namespace Drupal\spc_digital_library\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;

/**
 * Displays Key Statistic in a table
 *
 * @Block(
 *   id = "digital_library_documents",
 *   admin_label = @Translation("Digital Library Documents"),
 *   category = @Translation("SPC Digital Library")
 * )
 */
class DigitalLibraryDocuments extends BlockBase implements BlockPluginInterface {

  const DL_MEDIA_BUNDLE = 'digital_library_document';
  private $entityRepository;
    
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $items = [];
    if ($config['digital_library_document_nodes']) {
      $items = Media::loadMultiple($config['digital_library_document_nodes']);
    }

    $form['#tree'] = TRUE;

    $form['dl_items_intro'] = array(
      '#type' => 'text_format',
      '#title' => t('Introduction'),
      '#format'=> 'full_html',
      '#default_value' => empty($config['digital_library_document_introduction']) ? '' : $config['digital_library_document_introduction']['value']
    );

    $form['dl_items_layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#options' => [
        'dl-list'           => $this->t('List'),
        'dl-list-info'      => $this->t('List with file info'),
        'dl-grid'           => $this->t('Grid (auto)'),
        'dl-grid dl-grid-2' => $this->t('Grid 2 columns'),
        'dl-grid dl-grid-3' => $this->t('Grid 3 columns'),
        'dl-grid dl-grid-4' => $this->t('Grid 4 columns')
      ],
      '#default_value' => empty($config['digital_library_document_layout']) ? 'dl-list' : $config['digital_library_document_layout'],
      '#required' => true
    ];

    $form['dl_items_filename'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use short title (from filename)'),
      '#return_value' => 1,
      '#default_value' => empty($config['digital_library_document_shortname']) ? 0 : 1
    );

    $form['dl_items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('DL Documents'),
      '#prefix' => '<div id="dl-items-wrapper">',
      '#suffix' => '</div>',
    ];

    if (!$form_state->has('dl_items_count')) {
      $form_state->set('dl_items_count', count($config['digital_library_document_nodes']));
    }

    $name_field = $form_state->get('dl_items_count');

    for ($i = 0; $i < $name_field+1; $i++) {
      $items = array_values($items);
      $form['dl_items_fieldset']['items'][$i] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'media',
        '#selection_settings' => ['target_bundles' => [ 'digital_library_document' ] ],
        '#title' => t('Document'),
        '#selection_handler' => 'default',
        '#size' => 40,
        '#maxlength' => 512,
        '#default_value' => emptY($items[$i]) ? null : $items[$i]
      ];
    }

    $form['dl_items_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['dl_items_fieldset']['actions']['add_dl_doc_item'] = [
      '#type' => 'submit',
      '#value' => t('Add new item'),
      '#submit' => [[ $this, 'addOneDlDoc' ]],
      '#ajax' => [
        'callback' => [$this, 'refreshDocsCallback'],
        'wrapper' => 'dl-items-wrapper',
      ],
    ];

    if ($name_field > 1) {
      $form['dl_items_fieldset']['actions']['remove_dl_doc_item'] = [
        '#type' => 'submit',
        '#value' => t('Remove last'),
        '#submit' => [[$this, 'removeOneDlDoc']],
        '#ajax' => [
          'callback' => [$this, 'refreshDocsCallback'],
          'wrapper' => 'dl-items-wrapper',
        ]
      ];
    }

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addOneDlDoc(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('dl_items_count');
    $add_button = $name_field + 1;
    $form_state->set('dl_items_count', $add_button);
    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function removeOneDlDoc(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('dl_items_count');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('dl_items_count', $remove_button);
    }
    $form_state->setRebuild();
  }

  private function _searchFieldInForm($field, $form, $level=0) {
    foreach ($form as $key => $val) {
      if ($key === $field) {
        // key found
        return $val;
      } else if (is_array($val)) {
        if (!preg_match('/^#/', $key)) { // skip pseudo fields
          $res = $this->_searchFieldInForm($field, $val, $level++);
          if ($res) {
            // recursive search
            return $res;
          }
        }
      } else {
        // No match
      }
    }
    return false;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return mixed
   */
  public function refreshDocsCallback(array &$form, FormStateInterface $form_state) {
    // The form passed here is the entire form, not the subform that is
    // passed to non-AJAX callback.

    if (empty($form['settings'])) {
      // nested form (paragraphs)
      return $this->_searchFieldInForm('dl_items_fieldset', $form);
    } else {
      return $form['settings']['dl_items_fieldset'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    
    parent::blockSubmit($form, $form_state);

    foreach ($form_state->getValues() as $key => $value) {
      switch ($key) {
        case 'dl_items_fieldset':
          if (isset($value['items'])) {
            $items = $value['items'];
            foreach ($items as $key => $item) {
              if ($item === '' || !$item) {
                unset($items[$key]);
              }
            }
            $this->configuration['digital_library_document_nodes'] = $items;
          }
          break;
        case 'dl_items_layout':
          $this->configuration['digital_library_document_layout'] = $value;
          break;
        case 'dl_items_filename':
          $this->configuration['digital_library_document_shortname'] = $value;
          break;
        case 'dl_items_intro':
          $this->configuration['digital_library_document_introduction'] = $value;
          break;
      }
    }
    
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();

    $nodes = [];

    if (isset($config['digital_library_document_nodes'])) {
      if (count($config['digital_library_document_nodes']) > 0) {
        $nids = $config['digital_library_document_nodes'];
        $nodes = Media::loadMultiple($nids);
      }
    }

    // ---------- GENERATE HTML

    $pf = '<div class="digital-library-documents '.$config['digital_library_document_layout'].'">';
    $sf = '</div>';

    $html = '';

    $idx = 1;

    foreach ($nodes as $node) {
    
      $fileinfo = '';
      $filetitle = $node->get('field_dl_title')->getString();
      $thumb = $node->get('field_dl_thumbnail')->first()->getValue();
      
      $file = $node->get('field_dl_file')->first();
      $filename = $file->get('description')->getString();
      $fileext = substr($filename, strrpos($filename, '.') + 1);
      $fileid = $file->get('target_id')->getValue();

      $filenode = File::load($fileid);
      $filesize = format_size($filenode->get('filesize')->getString());
      $filemime = $filenode->getMimeType();

      if
        (
          (!empty($config['digital_library_document_shortname']) && !empty($filename))
          || !$filetitle
        )
      {
        $fileinfo = $filetitle;
        $filetitle = str_replace('_',' ',substr($filename, 0, strrpos($filename, '.')));
      }

      switch($filemime) {
        case 'application/pdf':
          $filemime = 'PDF';
          break;
        case 'application/zip':
        case 'application/vnd.rar':
        case 'application/gzip':
        case 'application/x-tar':
          $mimeparts = explode('/', $filemime);
          $filemime = 'Archive ('.$mimeparts[1].')';
          break;
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
          $filemime = 'MS Word'; 
          break;
        case 'application/vnd.ms-powerpoint':
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
          $filemime = 'Powerpoint'; 
          break;
        case 'application/vnd.ms-excel':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
          $filemime = 'Excel';
          break;
        default: 
          $mimeparts = explode('/', $filemime);
          switch ($mimeparts[0]) {
            case 'image':
              $filemime = 'Image ('.strtoupper($mimeparts[1]).')'; break;
            case 'text':
              $filemime = 'Text ('.strtoupper($mimeparts[1]).')'; break;
            case 'audio':
            case 'video':
              $filemime = ucfirst($mimeparts[0]);
              break;
          }
      }

      switch ($config['digital_library_document_layout']) {
        case 'dl-list':
          $fileinfo .= ($fileinfo?' - ':'').$filemime.', '.$filesize;
          $html .= '<p>'
            .'<span class="dl-icon dl-'.$fileext.'"><span class="dl-label">'.substr($fileext, 0, 3).'</span></span>'
            .'<span class="dl-title">'
            .'<a href="'.$filenode->createFileUrl().'" title="'.$fileinfo.'" target="_blank" rel="external nofollow">'
            .$filetitle
            .'</a>'
            .'</span>'
            .'</p>';
          break;
        case 'dl-list-info':
          $html .= '<p>'
            .'<span class="dl-icon dl-'.$fileext.'"><span class="dl-label">'.substr($fileext, 0, 3).'</span></span>'
            .'<span class="dl-title">'
            .'<a href="'.$filenode->createFileUrl().'" target="_blank" title="'.$fileinfo.'" rel="external nofollow">'
            .$filetitle
            .'</a>'
            .'<br /><small>'.$filemime.' - '.$filesize.'</small>'
            .'</span>'
            .'</p>';
          break;
        default:
          $fileinfo .= ($fileinfo?' - ':'').$filemime.', '.$filesize;
          $html .= '<figure>'
            .'<a href="'.$filenode->createFileUrl().'" target="_blank" title="'.$fileinfo.'" rel="external nofollow">'
            .'<div class="digital-library-entity"><img src="'.$thumb['uri'].'" alt="'.$thumb['title'].'" /></div>'
            .'<figcaption">'.$filetitle.'</figcaption>'
            .'</a>'
            .'</figure>';
          break;
      }
    }

    // --- PREPEND INTRO (if any)
    $intro = empty($config['digital_library_document_introduction']) ? '' : trim($config['digital_library_document_introduction']['value']);
    if (!empty($intro)) {
      $pf = '<div class="digital-library-introduction">'.$intro.'</div>'.$pf;
    }

    // --------------- RETURN MARKUP

    return [
      '#title' => t($config['label']),
      '#markup' => $html,
      '#prefix' => $pf,
      '#suffix' => $sf,
      '#attached' => [
        'library' => [
          'spc_digital_library/digital-library-css'
        ]
      ],
      '#cache' => [
        'max-age' => 3600, //caching chart for an hour
        // 'tags' => [ "node:{$node->id()}", "digital_library:document" ] // data entity dependent
      ]
    ];
    
  }

}
<?php

namespace Drupal\spc_digital_library\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\Plugin\media\Source\File;

/**
 * File entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "spc_digital_library",
 *   label = @Translation("SPC Digital Library"),
 *   description = @Translation("Use the SPC Digital library media."),
 *   allowed_field_types = {"file"},
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class DigitalLibrary extends File {

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {

    if ($attribute_name == 'thumbnail_uri' && !$media->get('field_dl_thumbnail')->isEmpty()) {
      return $media->get('field_dl_thumbnail')->getString();
    }

    return parent::getMetadata($media, $attribute_name);
  }

}

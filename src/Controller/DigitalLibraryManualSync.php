<?php

namespace Drupal\spc_digital_library\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\spc_digital_library\DigitalLibrarySync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Performs a manually triggered sync of digital library content.
 */
class DigitalLibraryManualSync extends ControllerBase {

  /**
   * The Digital Library Sync service.
   *
   * @var \Drupal\spc_digital_library\DigitalLibrarySync
   */
  private $digitalLibrarySync;

  /**
   * {@inheritdoc}
   */
  public function __construct(DigitalLibrarySync $digital_library_sync) {
    $this->digitalLibrarySync = $digital_library_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spc_digital_library.sync_service')
    );
  }

  /**
   * Perform a manual sync of digital library content.
   *
   * @return array
   *   Renderable array with results message.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function doSync() {
    $config = \Drupal::config('spc_digital_library.settings');
    $bucket = $config->get('spc_digital_library.bucket');
    $method = $config->get('spc_digital_library.method');
    if ($bucket && $method) {
      $this->digitalLibrarySync->syncAllRecords();
      return ['#markup' => 'Sync completed'];
    }
    else {
      return ['#markup' => 'Sync aborted - Please configure module at admin/config/media/spc_digital_library.'];
    }
  }

}

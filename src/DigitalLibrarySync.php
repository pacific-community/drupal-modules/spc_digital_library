<?php

namespace Drupal\spc_digital_library;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Synchronises document metadata from the Digital Library in to this site.
 */
class DigitalLibrarySync {


  const DL_MEDIA_BUNDLE = 'digital_library_document';

  /**
   * The url of the SPC digital library base.
   *
   * @var string
   */
  private $apiBase = 'https://www.spc.int/DigitalLibrary';


  /**
   * List of TAGS
   * @var array
   */
  private $tagList;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private $entityRepository;


  /**
   * The site language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * DigitalLibrarySync constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The guzzle http client.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ClientInterface $http_client, Connection $database, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager) {
    $this->client = $http_client;
    $this->database = $database;
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
  }

  /**
   * Gets the current settings for the service.
   *
   * @return array
   *   An array of settings.
   */
  public function getSettings() {
    $config = \Drupal::config('spc_digital_library.settings');
    return [
      'bucket'      => $config->get('spc_digital_library.bucket'),
      'method'      => $config->get('spc_digital_library.method'),
      'creation'    => $config->get('spc_digital_library.creation'),
      'multilang'   => $config->get('spc_digital_library.multilang'),
      'force_saves' => $config->get('spc_digital_library.force_saves'),
      'page_limit'  => intval($config->get('spc_digital_library.page_limit')),
      'page_size'   => 20
    ];
  }

  /**
   * List tags
   */
  protected function getTagList() {
    if (empty($this->tagList)) {
      $this->tagList = array();
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', 'tag');
      $query->sort('weight');
      $tids = $query->execute();
      $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
      foreach($terms as $term) {
        $this->tagList[$term->id()] = $term->getName();
      };
    }
    return $this->tagList;
  }

  /**
   * Collect all metadata records from the digital library.
   *
   * Each metadata record is saved as a media entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncAllRecords() {

    // Build a list of media entities we have already synced.
    $existing_uuids = $this->database->select('media', 'm')
      ->fields('m', ['uuid', 'mid'])
      ->condition('bundle', self::DL_MEDIA_BUNDLE)
      ->execute()->fetchAllKeyed();
    $sync_uuids = [];
    $saved_uuids = [];
    $skipped_uuids = [];
    $unpublished_uuids = [];

    $settings = $this->getSettings();

    $page = 1;
    do {
      $query = [
        'sort' => 'CreationDateDesc',
        'pageSize' => $settings['page_size'],
        'pageNumber' => $page++,
      ];
      $results = $this->request($settings['method'], $settings['bucket'], $query);

      // Try to sync each document in the webservice result.
      foreach ($results as $doc_record) {

        $sync_uuids[] = $doc_record['id'];
        try {
          $media = $this->syncRecord($doc_record);
          $uuid = $media->get('uuid')->getString();
          if (isset($media->skipped)) {
            $skipped_uuids[] = $uuid;
          } else if (isset($media->deleted)) {
            $unpublished_uuids[] = $uuid;
          } else {
            $saved_uuids[] = $uuid;
          }
        }
        catch (\Exception $ex) {
          \Drupal::logger('spc_digital_library')->error($ex->getMessage());
        }
      }

      if (!empty($settings['page_limit']) && $page > $settings['page_limit']) {
        break;
      }

    } while (!empty($results));

    // Don't do any unpublishing if we didn't sync everything correctly.
    if (count($sync_uuids) != (count($saved_uuids) + count($skipped_uuids))) {
      \Drupal::logger('spc_digital_library')->error("Not all records were synced, skipping un-publish step");
      return;
    }
    else if (empty($settings['page_limit'])) {
      // For any records which weren't found in the webservice request,
      // un-publish the record.
      foreach ($existing_uuids as $uuid => $mid) {
        if (!in_array($uuid, $sync_uuids)) {
          $media = Media::load($mid);
          $media->setUnpublished();
          $media->save();
          $unpublished_uuids[] = $uuid;
        }
      }
    }

    // reset page limit if needed
    /*
    if ($settings['page_limit'] > 0) {
      $config = $this->config('spc_digital_library.settings');
      $config->set('spc_digital_library.page_limit', '');
    }
    */

    // disable force update when done
    if ($settings['force_saves']) {
      $config = \Drupal::service('config.factory')->getEditable('spc_digital_library.settings');
      $config->set('spc_digital_library.force_saves', 0);
      $config->save();
    }

    \Drupal::logger('spc_digital_library')->info("Finished sync. Found @records records, saved @saved, skipped @skipped (because no changes) unpublished @unpublished", [
      '@records' => count($sync_uuids),
      '@saved' => count($saved_uuids),
      '@skipped' => count($skipped_uuids),
      '@unpublished' => count($unpublished_uuids),
    ]);

    \Drupal::messenger()->addStatus(t("Finished sync. Found @records records, saved @saved, skipped @skipped (because no changes) unpublished @unpublished", [
      '@records' => count($sync_uuids),
      '@saved' => count($saved_uuids),
      '@skipped' => count($skipped_uuids),
      '@unpublished' => count($unpublished_uuids),
    ]));
  }

  /**
   * Sync a single record from the digital library.
   *
   * @param array $doc_record
   *   The record formatted as an array structured as a hit from the
   *   GetCollectionDocumentsForCountry method.
   *
   * @return \Drupal\media\Entity\Media
   *   The saved media entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function syncRecord(array $doc_record) {

    $media_files = [];
    $media_thumbs = [];

    $settings = $this->getSettings();

    $uuid = $doc_record['id'];

    $media = $this->entityRepository->loadEntityByUuid('media', $uuid);
    if (!$media) {
      $media = Media::create(['bundle' => self::DL_MEDIA_BUNDLE]);
      if (count($doc_record['documents']) < 1) {
        // ignore reference with no docs
        $media->skipped = TRUE;
        return $media;
      }
    } else {
      if (!$settings['force_saves'] && json_decode($media->get('field_dl_record')->getString(), TRUE) == $doc_record) {
        if (count($doc_record['documents'])) {
          if (!$media->isPublished()) {
            $media->setPublished();
            $media->save();
          } else {
            $media->skipped = TRUE;
          }
        } else {
          // no files? mark as deleted
          $media->save();
          $media->deleted = TRUE;
        }
        return $media;
      } else if ($media->isPublished() && !count($doc_record['documents'])) {
        // unpublish records with no files
        // $media->setUnpublished(); # no need, will be taken care of by spc_digital_library_media_presave() hook
        $media->save();
        $media->deleted = TRUE;
        return $media;
      }
    }

    // Save the reference to the file on the document record to the db.
    $files = $this->saveDocumentFiles($doc_record);

    $entname = $doc_record['title'];

    foreach ($files as $file) {
      $media_files[] = [
        'target_id'   => $file->id(),
        'description' => $file->get('filename')->getString()
      ];
      /*
      try {
        if (!$entname) {
          if ($entarr = $file->get('filename')->getValue()) {
            $entname = array_shift(array_shift($entarr));
            $entname = str_replace('_',' ',substr($entname, 0, strrpos($entname, '.')));
          }
        }
      } catch (Exception $e) {
        $entname = $doc_record['title'];
      }
      */
    }

    $lastModified = '1900-01-01';

    foreach ($doc_record['documents'] as $dl_doc) {
      $media_thumbs[] = ['uri' => $dl_doc['thumbnail']];
      if ($dl_doc['lastModified'] > $lastModified) {
        $lastModified = $dl_doc['lastModified'];
      }
    }

    $media->set('uuid', $uuid);
    $media->set('uid', 0);
    $media->set('name', Unicode::truncate($entname, 254, TRUE, TRUE));
    $media->set('status', 1);
    $media->set('field_dl_title', $doc_record['title']);
    $media->set('field_dl_authors', $doc_record['authors']);
    $media->set('field_dl_year', $doc_record['year']);

    $lastModDate = substr($lastModified, 0, 10);
    switch($settings['creation']) {
      case 'year':
        if (!empty($doc_record['year'])) {
          $media->set('field_dl_created_date', $doc_record['year'] . substr($lastModDate, 4));
        }
        else {
          $media->set('field_dl_created_date', $lastModDate);
        }
        break;
      default:
        $media->set('field_dl_created_date', $lastModDate);
        break;
    }
    
    $media->set('field_dl_reference', $doc_record['reference']);
    $media->set('field_dl_record', json_encode($doc_record));
    $media->set('field_dl_file', $media_files);
    $media->set('field_dl_thumbnail', $media_thumbs);

    $country_field_values = [];
    foreach ($doc_record['countries'] as $delta => $value) {
      $country_field_values[] = ['value' => $value['code']];
    }
    $media->set('field_dl_countries', $country_field_values);

    $multilingual_fields = [
      'domainKeywords:name' => 'field_dl_domain_keywords',
      'domainKeywords:abbrev' => 'field_dl_domain_keywords_abbr',
      'keywords:name' => 'field_dl_keywords',
      'collections:name' => 'field_dl_collections',
      'collections:code' => 'field_dl_collection_code',
    ];

    // load up list of terms
    $this->getTagList();

    foreach ($multilingual_fields as $source => $field_name) {

      list($source_key, $subkey) = explode(":", $source);

      $field_values = [];
      $termIds = [];

      foreach ($doc_record[$source_key] as $delta => $value) {
        if (isset($value[$subkey]) && !empty($value[$subkey])) {
          $field_values[$delta] = ['value' => $value[$subkey]];
        }
        else {
          $value_key = $subkey . '_EN';
          if (isset($value[$value_key]) && !empty($value[$value_key])) {
            $field_values[$delta] = ['value' => $value[$value_key]];
          }
        }
      }

      $media->set($field_name, $field_values);

    }

    /*
    // Deal with any multilingual data and set up translated media entities.
    /** @var \Drupal\Core\Language\LanguageInterface $language */

    $arlangs = $this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE);

    if ($settings['multilang'] && $arlangs > 1) {

      foreach ($this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE) as $language) {

        $langcode = $language->getId();
        $translated_media = NULL;

        foreach ($multilingual_fields as $source => $field_name) {

          list($source_key, $subkey) = explode(":", $source);

          $field_values = [];
          foreach ($doc_record[$source_key] as $delta => $value) {

            if (isset($value[$subkey]) && !empty($value[$subkey])) {
              $field_values[$delta] = ['value' => $value[$subkey]];
            }
            else {
              $value_key = $subkey . '_' . strtoupper($langcode);
              if (isset($value[$value_key]) && !empty($value[$value_key])) {
                $field_values[$delta] = ['value' => $value[$value_key]];
              }
            }

          }

          if (!empty($field_values)) {
            if ($langcode != $media->language()->getId()) {

              if ($media->hasTranslation($langcode)) {
                #** @var \Drupal\media\Entity\MediaInterface $translated_media *#
                $translated_media = $media->getTranslation($langcode);
              }
              else {
                $translated_media = $media->addTranslation($langcode, $media->toArray());
              }

              $translated_media->set($field_name, $field_values);
            }
            else {
              $media->set($field_name, $field_values);
            }
          }
        }

        // If we had a translation, save it.

        if (!empty($translated_media)) {
          $translated_media->save();
        }

      }
    
    }

    /*
    # NOTE: if more fields need to be populated
    # implement entity_presave hook in another module
    # example : 
    function hook_entity_presave(EntityInterface $entity) {
      if ($entity->getEntityTypeId() == 'media' 
        && $entity->bundle() == 'digital_library_document') {
        // do more i.e. match tags, countries, etc.
      }
    }
    
    */

    // Save the media entity.
    $media->save();

    return $media;
  }

  /**
   * Syncs any files in the doc_record array.
   *
   * @param array $doc_record
   *   The array of structured data returned from a hit in the
   *   GetCollectionDocumentsForCountry method.
   *
   * @return \Drupal\file\Entity\File[]
   *   An array of saved file entities
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveDocumentFiles(array $doc_record) {

    $files = [];
    foreach ($doc_record['documents'] as $dl_doc) {
      $file = $this->entityRepository->loadEntityByUuid('file', $dl_doc['id']);
      if (empty($file)) {
        $file = File::create();
      }

      $file->set('uuid', $dl_doc['id']);
      $file->set('uid', 0);
      $file->set('status', 1);
      $file->set('uri', $dl_doc['link']);
      $file->set('filemime', $dl_doc['mimeType']);
      $file->set('filesize', $dl_doc['fileSize']);
      $file->set('filename', $dl_doc['fileName']);
      
      // Save the file entity without using $file->save().
      $this->saveFileRecordDirectly($file);
      $files[] = $file;
    }

    return $files;
  }

  /**
   * Saves a file record bypassing the Entity API.
   *
   * This is basically a performance hack, because an entity save will do much
   * more work when the remote_stream_wrapper module tries to get file headers.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file being saved.
   *
   * @throws \Exception
   */
  protected function saveFileRecordDirectly(FileInterface $file) {

    $fields = [];
    foreach ($file->getFields(FALSE) as $field_name => $field) {
      $value = $field->getString();
      if (!empty($value)) {
        $fields[$field_name] = $field->getString();
      }
    }

    if (!$file->id()) {
      unset($fields['fid']);
    }

    $fid = $this->database->select('file_managed', 'f')
      ->fields('f', ['fid'])
      ->condition('uuid', $fields['uuid'])
      ->execute()
      ->fetchField();

    if (!$fid) {
      $this->database->insert('file_managed')->fields($fields)->execute();
      $fid = $this->database->select('file_managed', 'f')
        ->fields('f', ['fid'])
        ->condition('uuid', $fields['uuid'])
        ->execute()
        ->fetchField();
    }
    else {
      $this->database->update('file_managed')->condition('fid', $fid)->fields($fields)->execute();
    }

    if (!$file->id()) {
      $file->set('fid', $fid);
    }
  }

  /**
   * Perform a webservice GET request on the Digital Library.
   *
   * @param string $method
   *   The webservice method.
   * @param string $bucket
   *   The Digital Library Bucket e.g. FAME or SDD.
   * @param array $query
   *   Array of url query parameters to use e.g. pageNumbers etc.
   *
   * @return array
   *   The result of the webservice request at the 'hits' key.
   */
  protected function request($method, $bucket, array $query) {
    $method_url = $this->apiBase . '/api/' . $method . '/' . $bucket;
    $method_options = ['query' => $query];
    $uri = Url::fromUri($method_url, $method_options)->toString();
    $raw_result = $this->client->get($uri)->getBody()->getContents();

    $results = json_decode($raw_result, TRUE);
    if (isset($results['hits'])) {
      return $results['hits'];
    }
    return [];
  }

}

<?php

namespace Drupal\spc_digital_library\Commands;

use Drupal\spc_digital_library\DigitalLibrarySync;
use Drush\Commands\DrushCommands;

class DigitalLibrarySyncCommands extends DrushCommands {

  /**
   * The Digital Library Sync service.
   *
   * @var \Drupal\spc_digital_library\DigitalLibrarySync
   */
  private $digitalLibrarySync;

  /**
   * {@inheritdoc}
   */
  public function __construct(DigitalLibrarySync $digital_library_sync) {
    parent::__construct();
    $this->digitalLibrarySync = $digital_library_sync;
  }

  /**
   * Collect all metadata records from the digital library.
   *
   * @validate-module-enabled spc_digital_library
   *
   * @command digital-library:sync
   *
   * @aliases dls,digital-library-sync
   */
  public function syncDigitalLibrary() {
    $config = \Drupal::config('spc_digital_library.settings');
    $bucket = $config->get('spc_digital_library.bucket');
    $method = $config->get('spc_digital_library.method');
    if ($bucket && $method) {
      $this->output()->writeln('Syncing from digital library...');
      $this->digitalLibrarySync->syncAllRecords();
      $this->output()->writeln('Sync completed.');
    }
    else {
      $this->output()->writeln('Sync aborted - Please configure module at admin/config/media/spc_digital_library.');
    }
  }

}

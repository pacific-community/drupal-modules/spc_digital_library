<?php

namespace Drupal\spc_digital_library\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class DigitalLibraryForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spc_digital_library_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('spc_digital_library.settings');

    // General settings (fieldset)
    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
      '#required' => true
    ];

    // Bucket name.
    $form['general']['bucket'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Digital Library Bucket'),
      '#default_value' => $config->get('spc_digital_library.bucket'),
      '#required' => true
    ];

    // Method.
    $form['general']['method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Digital Library API method'),
      '#default_value' => $config->get('spc_digital_library.method'),
      '#required' => true
    ];

    // Date field.
    $form['general']['creation'] = [
      '#type' => 'select',
      '#title' => $this->t('Creation date field'),
      '#options' => [
        'lastModified'  => $this->t('Last modification date'),
        'year'          => $this->t('Year')
      ],
      '#default_value' => $config->get('spc_digital_library.creation'),
      '#required' => true
    ];

    // Multi language
    $form['multiset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multi language support')
    ];
    $form['multiset']['multilang'] = [
      '#type' => 'checkbox',
      '#title' => t('Create separate media entities for each supported language'),
      '#default_value' => $config->get('spc_digital_library.multilang'),
      '#description' => t('Warning: Requires translations to be enabled for <em>Digital Library Documents</em> media type.')
    ];

    // Developper options
    $form['development'] = array(
      '#type' => 'details',
      '#title' => $this->t('Development')
    );

    // limit number of pages
    $form['development']['page_limit'] = [ 
      '#type' => 'number',
      '#title' => $this->t('Page limit'),
      '#default_value' => $config->get('spc_digital_library.page_limit'),
      '#description' => 'Leave empty to parse all rows/pages.'
    ];

    // Force update
    $form['development']['force_saves'] = [ 
      '#type' => 'checkbox',
      '#title' => $this->t('Force entities updates'),
      '#default_value' => $config->get('spc_digital_library.force_saves'),
      '#description' => t('Tick to update all DL documents on next synchronisation.')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $raw = $form_state->getValue('page_limit');
    if (!empty($raw) && intval($raw) != (int) $raw) { 
      $form_state->setErrorByName('page_limit', $this->t('Page limit needs to be a number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('spc_digital_library.settings');
    $config->set('spc_digital_library.bucket', $form_state->getValue('bucket'));
    $config->set('spc_digital_library.method', $form_state->getValue('method'));
    $config->set('spc_digital_library.creation', $form_state->getValue('creation'));
    $config->set('spc_digital_library.multilang', empty($form_state->getValue('multilang'))?0:1);
    $config->set('spc_digital_library.page_limit',
      empty($form_state->getValue('page_limit'))?'':intval($form_state->getValue('page_limit')));
    $config->set('spc_digital_library.force_saves', 
      empty($form_state->getValue('force_saves'))?0:1);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spc_digital_library.settings',
    ];
  }

}
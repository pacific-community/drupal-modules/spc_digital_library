# SPC Digital Library

Drupal 8/9 module to synchronize documents from SPC Digital Library

## Installing

### Download using composer

Setup Git repository
```
composer config repositories.spc_digital_library vcs \
 git@gitlab.com:pacific-community/drupal-modules/spc_digital_library.git
```

Download module
```
composer require pacific_community/spc_digital_library:^1.0
```

### Drupal 9 issue

There's an issue with Remote Stream Wrapper which has been reported  
See https://www.drupal.org/project/remote_stream_wrapper/issues/3185015

Update composer.json with the following to patch remote streamer wrapper:
```
{
  "require": {
    "cweagans/composer-patches": "^1.7"
  },
  "extra": {
    "patches": {
      "drupal/remote_stream_wrapper": {
        "Remote Stream Wrapper": "https://www.drupal.org/files/issues/2021-05-10/3185015-29.patch"
      }
    }
  }
}
```

### Enable module

Through admin GUI or using drush:
```
drush pm-enable spc_digital_library
```

### Setup

Configure options on module's configuration page at _admin/config/media/spc_digital_library_.

* __Bucket__: the DL bucket e.g. `FAME` or `SDD`
* __API method__: the webserver method to call e.g. `GetCollectionDocumentsForCountry`
* __Creation date__: select which field provides creation date of the DL document


## Version History

* 1.2.1 : Ignore meta data entries with no files from API response (unpublish if existing)
* 1.2.0 : Adjust entities' revisionCreatedTime and changedTime from original upload and last update done in the DL
* 1.1.9 : When ordering by year, complete the date from the upload date month and day
* 1.1.8 : Improve list of files style and icons (selected DL documents block)
* 1.1.7 : Adding file icons in selected DL documents blocks (lists layout)
* 1.1.6 : Enable or disable media type translation (by configuration)
* 1.1.5 : Drush command and translatable block label
* 1.1.4 : Better file information (title attribute)
* 1.1.3 : Documents short titles from filename support
* 1.1.2 : Paragraph compatibility fix
* 1.1.1 : Block category fix
* 1.1.0 : Block: listing specific documents 
* 1.0.3 : Creation date field option
* 1.0.2 : File and Thumbnail fields renamed to avoid conflict with other modules
* 1.0.1 : Developer options
* 1.0.0 : Initial Release
